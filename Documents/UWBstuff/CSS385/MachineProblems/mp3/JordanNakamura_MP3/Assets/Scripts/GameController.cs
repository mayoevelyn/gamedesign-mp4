﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public float ShotFrequency;
    public float SpawnFrequency;
    public int NumEnemiesStart;
    public float WorldHeight;
    public float WorldWidth;
    public Text DisplayEnemyCount;
    public Text DisplayEggCount;

    private AudioSource source;
    private bool mIsMovementOn;
    private GameObject mHero;
    private GameObject mEnemy;
    private float mTimeSinceLastShot;
    private float mTimeSinceLastSpawn;
    // Use this for initialization
    void Start()
    {
        mTimeSinceLastShot = mTimeSinceLastSpawn = Time.time;
        mHero = GameObject.Find("Hero");
        source = GetComponent<AudioSource>();

        for (int i = 0; i < NumEnemiesStart; i++)
        {
            SpawnEnemy(false);
        }

        mIsMovementOn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (mIsMovementOn && !source.isPlaying)
        {
            source.Play();
        }
        else if (!mIsMovementOn && source.isPlaying)
        {
            source.Stop();
        }

        if (Input.GetButton("Fire1"))
        {
            SpawnEgg();
        }
        UpdateText();
        UpdateMovement();
        if (mIsMovementOn && (Time.time - mTimeSinceLastSpawn >= SpawnFrequency))
        {
            SpawnEnemy(true);
            mTimeSinceLastSpawn = Time.time;
        }
    }

    void UpdateText()
    {
        GameObject[] enemyCount = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] eggCount = GameObject.FindGameObjectsWithTag("Egg");

        DisplayEnemyCount.text = "Enemies in world: " + enemyCount.Length;
        DisplayEggCount.text = "Eggs in world: " + eggCount.Length;
    }

    void SpawnEgg()
    {
        if (Time.time - mTimeSinceLastShot >= ShotFrequency)
        {
            GameObject newEgg = Instantiate((GameObject)Resources.Load("Prefabs/Egg"),
                mHero.transform.position, mHero.transform.rotation) as GameObject;
            newEgg.SetActive(true);
            mTimeSinceLastShot = Time.time;
        }
    }
    void SpawnEnemy(bool move)
    {
        mEnemy = Instantiate((GameObject)Resources.Load("Prefabs/Enemy"),
                GetRandomVector2(), mHero.transform.rotation);
        mEnemy.transform.Rotate(Vector3.forward * Random.value * 360);
        mEnemy.SetActive(true);
        if (move)
        {
            EnemyController other = mEnemy.GetComponent<EnemyController>();
            other.Move();
        }
    }
    Vector2 GetRandomVector2()
    {
        return new Vector2(Random.value * WorldWidth - (WorldWidth / 2), Random.value * WorldHeight - (WorldHeight / 2));
    }
    void UpdateMovement()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            mIsMovementOn = !mIsMovementOn;
        }
    }
}
