﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggController : MonoBehaviour
{
    public int EggSpeed;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * Time.smoothDeltaTime * EggSpeed, Space.Self);

        if (!IsInBounds())
        {
            gameObject.SetActive(false);
        }
    }

    bool IsInBounds()
    {
        return (transform.position.x < 150 && transform.position.x > -150
            && transform.position.y < 120 && transform.position.y > -120);
    }
}
