﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    public float MovementSpeed;
    public float RotationSpeed;
    public int BoundLeft;
    public int BoundRight;
    public int BoundLower;
    public int BoundUpper;

    private Animator mAnime;
    // Use this for initialization
    void Start()
    {
        mAnime = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float movement = Input.GetAxis("Vertical");
        float rotation = Input.GetAxis("Horizontal");

        if (movement != 0)
        {
            mAnime.enabled = true;
            transform.Translate(Vector3.up * Time.smoothDeltaTime * MovementSpeed * movement, Space.Self);
        }
        else
        {
            mAnime.enabled = false;
        }
        if (rotation != 0)
        {
            transform.Rotate(Vector3.forward * Time.smoothDeltaTime * RotationSpeed * -rotation);
        }
    }
}
