﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private enum EnemyState
    {
        NormalState = 0,
        RunState = 1,
        StunnedState = 2,
        DefeatedState = 3
    };

    public float EnemySpeedLower;
    public float EnemySpeedUpper;
    public float EnemyTurnSpeed;
    public int HitsToDie;
    public int StunTime;
    public AudioClip hitSound;
    public AudioClip defeatedSound;

    private AudioSource source;
    private bool mMovementOn;
    private float mTimeHit;
    private EnemyState mCurrentState;
    private Animator mAnime;
    private GameObject mHero;
    private RuntimeAnimatorController mControlRun;
    private RuntimeAnimatorController mControlStunned;
    private RuntimeAnimatorController mControlWalk;
    // Use this for initialization
    void Start()
    {
        mHero = GameObject.Find("Hero");
        mAnime = GetComponent<Animator>();
        mCurrentState = EnemyState.NormalState;
        mControlRun = Resources.Load("Animations/EnemyRun") as RuntimeAnimatorController; ;
        mControlStunned = Resources.Load("Animations/EnemyStunned") as RuntimeAnimatorController;
        mControlWalk = Resources.Load("Animations/Enemy") as RuntimeAnimatorController;
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMovementStatus();
        UpdateState();
        ExecuteState();
    }

    void UpdateMovementStatus()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            mMovementOn = !mMovementOn;
        }
    }

    void UpdateState()
    {
        if (mCurrentState != EnemyState.DefeatedState)
        {
            if (mCurrentState == EnemyState.StunnedState && (Time.time - mTimeHit <= 5))
            {
                mCurrentState = EnemyState.StunnedState;
            }
            else if (Vector3.Distance(transform.position, mHero.transform.position) <= 30)
            {
                mCurrentState = EnemyState.RunState;
            }
            else if (mCurrentState != EnemyState.DefeatedState)
            {
                mCurrentState = EnemyState.NormalState;
            }
        }
    }

    void ExecuteState()
    {
        switch (mCurrentState)
        {
            case EnemyState.NormalState:
                EnemyMove();
                break;
            case EnemyState.RunState:
                EnemyRetreat(false);
                break;
            case EnemyState.StunnedState:
                EnemyStunned();
                break;
            case EnemyState.DefeatedState:
                EnemyRetreat(true);
                CheckBounds();
                break;
            default:
                break;
        }
    }

    void EnemyMove()
    {
        mAnime.runtimeAnimatorController = mControlWalk;

        if (mMovementOn)
        {
            mAnime.enabled = true;
            transform.Translate(Vector3.up * Time.smoothDeltaTime *
                Random.Range(EnemySpeedLower, EnemySpeedUpper), Space.Self);
        }
        else
        {
            mAnime.enabled = false;
        }
        if (!IsInWorldBounds())
        {
            Debug.Log("collided position: " + transform.position);
            NewDirection();
        }
    }
    void EnemyRetreat(bool defeated)
    {
        mAnime.runtimeAnimatorController = mControlRun;
        mAnime.enabled = true;
        if (!IsInWorldBounds() && !defeated)
        {
            Debug.Log("collided position: " + transform.position);
            NewDirection();
        }

        Vector3 targetDir3D = mHero.transform.position - transform.position;
        if (defeated)
        {
            targetDir3D = Vector3.zero - transform.position;
        }
        Vector2 targetDir2D = new Vector2(targetDir3D.x, targetDir3D.y);
        targetDir2D.Normalize();

        float theta = (float)((180.0 / Mathf.PI) *
                Mathf.Acos(Vector2.Dot(transform.forward, targetDir2D)));

        if (theta > 0.001f)
        {
            Vector3 sign = Vector3.Cross(transform.up, targetDir3D);

            transform.Rotate(new Vector3(0, 0, -Mathf.Sign(sign.z) * theta * EnemyTurnSpeed));
            transform.Translate(Vector3.up * Time.smoothDeltaTime * EnemySpeedUpper, Space.Self);
        }
    }
    void EnemyStunned()
    {
        mAnime.runtimeAnimatorController = mControlStunned;
        mAnime.enabled = true;
        transform.Rotate(Vector3.forward * Time.smoothDeltaTime * 9);
    }
    bool IsInWorldBounds()
    {
        return transform.position.x >= -115 && transform.position.x <= 115
            && transform.position.y >= -100 && transform.position.y <= 100;
    }
    private void NewDirection()
    {
        // we want to move towards the center of the world
        Vector2 v = Vector2.zero - new Vector2(transform.position.x, transform.position.y);
        // this is vector that will take us back to world center
        v.Normalize();
        Vector2 vn = new Vector2(v.y, -v.x); // this is a direction that is perpendicular to V

        float useV = 1.0f - Mathf.Clamp(0.5f, 0.01f, 1.0f);
        float tanSpread = Mathf.Tan(useV * Mathf.PI / 2.0f);

        float randomX = Random.Range(0f, 1f);
        float yRange = tanSpread * randomX;
        float randomY = Random.Range(-yRange, yRange);

        Vector2 newDir = randomX * v + randomY * vn;
        newDir.Normalize();
        transform.up = newDir;

        transform.Translate(transform.up * 0.1f, Space.World);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Hit!");

        // Only care if hitting an Egg (vs. hitting another Enemy!)
        if (other.gameObject.name == "Egg(Clone)")
        {
            HitsToDie--;
            if (HitsToDie <= 0)
            {
                source.PlayOneShot(defeatedSound, 1f);
                Destroy(other.gameObject);
                mCurrentState = EnemyState.DefeatedState;
            }
            else
            {
                Destroy(other.gameObject);
                mCurrentState = EnemyState.StunnedState;
                mTimeHit = Time.time;
                source.PlayOneShot(hitSound, 1f);
            }
        }
    }
    void CheckBounds()
    {
        if (!(transform.position.x < 150 && transform.position.x > -150
            && transform.position.y < 120 && transform.position.y > -120))
        {
            Destroy(this.gameObject);
        }
    }
    public void Move()
    {
        mMovementOn = true;
    }
}
