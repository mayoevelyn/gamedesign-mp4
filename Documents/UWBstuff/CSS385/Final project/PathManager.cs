﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathManager : MonoBehaviour {

	/*
    private enum PathState
    {
        Moving,
        Rotating,
        Stop
    }

    private PathState state;

    public int pathNumber;
    public Vector3 startPoint;
    public Vector3 moveTo;
    private Vector3 target;

    public float enemySpeed = 0.1f;
    private float enemyRotateSpeed = 180f / 2f; // 180-degrees in 2 seconds
    private float rotationTime;
    private float timeSinceRotationStart;

    public float[] path1X;
    public float[] path1Y;

    public bool returning;
    public bool enemyOnTrack;
    private bool rotating;

    public GameObject enemy;

    // Use this for initialization
    void Start () {
        transform.position = startPoint;
        target = moveTo;
        returning = true;
        enemyOnTrack = true;
        state = PathState.Moving;
	}
	
	// Update is called once per frame
	void Update () {
        // update based on state
        switch (state)
        {
            case PathState.Moving:
                updateMoving();
                break;
            case PathState.Rotating:
                updateRotating();
                break;
            case PathState.Stop:
                break;
        }
    }

    void updateMoving()
    {
        // if at end of path, rotate and reverse direction
        if (transform.position == target)
        {
            timeSinceRotationStart = Time.time;
            state = PathState.Rotating;
            return;
        }
        
        // move towards target
        Vector3 toTarget = (target - transform.position).normalized;
        transform.position += (enemySpeed * Time.smoothDeltaTime) * toTarget;
    }

    void updateRotating()
    {
        if (Time.time - timeSinceRotationStart > rotationTime)
        {
            rotating = true;
            float angle = enemyRotateSpeed * Time.smoothDeltaTime;
            transform.Rotate(transform.forward, angle);
        }
        else
        {
            rotating = false;
            if (target == moveTo)
                target = startPoint;
            else if (target == startPoint)
                target = moveTo;
        }
    }
    */

	public float pathLength;
	public float enemySpeed = 0.1f;
	private float enemyRotateSpeed = 180f / 2f; // 180-degrees in 2 seconds

	public bool returning;
	public bool enemyOnTrack;
	private bool rotating;

	public int pathNumber;

	public Vector3 startPoint;
	private Vector3 target;
	private Vector3 targetDir;
	private float distance;

	public GameObject enemy;

	void Start () {
		pathLength = 20f;
		startPoint = transform.position;
		returning = false;
		enemyOnTrack = true;

		float chooser = Random.value;

		pathNumber = 1;
		/*
		if (chooser > 0.5f) {
			pathNumber = 1;
		} else {
			pathNumber = 2;
		}
		*/

		target = new Vector3 (startPoint.x, startPoint.y + pathLength);
		distance = Vector3.Distance (startPoint, target);
		targetDir = target - transform.position;
	}

	void Update() {
		if (pathNumber == 1) {
			Debug.Log ("enter path number 1");
			transform.up = targetDir;
			Debug.Log ("transform.up: " + transform.up);
			if (distance < pathLength && !returning) {
				transform.position += (enemySpeed * Time.smoothDeltaTime) * transform.up;
			} else if (distance == pathLength) {
				returning = true;
			} else if (returning) {
				transform.position -= (enemySpeed * Time.smoothDeltaTime) * transform.up;
			} else if (transform.position == startPoint && returning) {
				returning = false;
				distance = 0;
			}
		}
	}
}
