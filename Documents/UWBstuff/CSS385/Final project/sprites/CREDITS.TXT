###############################################
OpenGameArt.org automatically generated credits file notice:

THIS FILE IS FOR INFORMATIONAL PURPOSES ONLY.  NO WARRANTY OF ANY KIND IS EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT. IN NO EVENT SHALL OPENGAMEART.ORG BE LIABLE FOR ANY CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT, OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE USE OF THIS FILE OR THE INFORMATION IN IT.

In plain English:  This credits file is automatically generated to the best of our ability, but it remains YOUR RESPONSIBILITY as a game developer to ensure that the art you use is attributed properly.  That is, we can't be held liable even if all of the information in this file is completely wrong.   In addition, some of the art listed in this file may have SPECIAL ATTRIBUTION INSTRUCTIONS that ask that you credit the artist in a particular way, rather than a verbatim copyright notice.  In those cases, you will probably need to hand-edit this file to comply with their instructions.  In some older art, attribution instructions may be included in the art description itself.  Please check the art page to be sure.

We recommend that you delete this notice when you're sure the information here is correct and the attribution instructions are properly followed.  If you aren't the game developer and you're seeing this notice here in the credits, it probably means that the developer hasn't properly verified the attribution information contained herein even though it said they were supposed to do it right on the page where they downloaded this file.  
###############################################

Title:
    16x16 Wall set

Author:
    InanZen

URL:
    https://opengameart.org/content/16x16-wall-set

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

SPECIAL ATTRIBUTION INSTRUCTIONS:
    Link to http://opengameart.org/

File(s):
    * WallSet.png

----------------------------------------

Title:
    16x16 Town Remix

Author:
    Sharm

Collaborators:
    Redshrike, surt

URL:
    https://opengameart.org/content/16x16-town-remix

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Art by Lanea "Sharm" Zimmerman, Stephen "Redshrike" Challener, Carl "Surt" Olsson, and Jetrel, for OpenGameArt.org (http://opengameart.org)

File(s):
    * 16oga.png

----------------------------------------

Title:
    16x16 Overworld Tiles

Author:
    Sharm

URL:
    https://opengameart.org/content/16x16-overworld-tiles

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * OGA-BY 3.0 ( http://static.opengameart.org/OGA-BY-3.0.txt )

File(s):
    * set.gif

----------------------------------------

Title:
    16x16 Icon Pack - Fans

Author:
    DatapawWolf

URL:
    https://opengameart.org/content/16x16-icon-pack-fans

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

SPECIAL ATTRIBUTION INSTRUCTIONS:
    If used, I&#039;d appreciate a little note like:
"Icons by DatapawWolf @http://datapaw.com"
But this is solely up to you. :)

File(s):
    * 10 Fan Icon Pack.zip

----------------------------------------

Title:
    Bitmap Font Pack

Author:
    ZiNGOT

URL:
    https://opengameart.org/content/bitmap-font-pack

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

SPECIAL ATTRIBUTION INSTRUCTIONS:
    Zingot Games for accreditation and a mention of www.zingot.com if that&#039;s ok :)

File(s):
    * font-pack.zip

----------------------------------------

Title:
    Overworld tiles

Author:
    Buch

Collaborators:
    bukketgames

URL:
    https://opengameart.org/content/overworld-tiles-0

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Credit me as Buch and link back either to my OGA profile page or to http://blog-buch.rhcloud.com, and credit Jeffrey Kern as the committer and creative consultant

File(s):
    * overworld.png

----------------------------------------

Title:
    The Field of the Floating Islands

Author:
    Buch

Collaborators:
    usr_share, surt

URL:
    https://opengameart.org/content/the-field-of-the-floating-islands

License(s):
    * CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )

Copyright/Attribution Notice:
    NOT REQUIRED, though appreciated: credit me as Buch and link back either to my OGA profile page or to http://blog-buch.rhcloud.com

File(s):
    * tiles.png
    * tiles_packed.png
    * snow-expansion.png

----------------------------------------

Title:
    5 MORE RPG/Fantasy Weapons!

Author:
    Cerbion

URL:
    https://opengameart.org/content/5-more-rpgfantasy-weapons

License(s):
    * CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )

Copyright/Attribution Notice:
    Not mandatory but it would be nice if you credit me "Cerbion"

File(s):
    * 5rpgweapons2.zip

----------------------------------------

Title:
    5 RPG / Fantasy Weapons

Author:
    Cerbion

URL:
    https://opengameart.org/content/5-rpg-fantasy-weapons

License(s):
    * CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )

Copyright/Attribution Notice:
    This work is CC0 but if you like you can credit me "Cerbion" :)

File(s):
    * 5rpgweapons.zip

----------------------------------------

Title:
    16x16 JRPG tilesets enlarged 2x

Author:
    Multiple Artists (see description)

URL:
    https://opengameart.org/content/16x16-jrpg-tilesets-enlarged-2x

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Cave, desert, sewer, and overworld sets by MrBeast.  Interior tileset by RedShrike.  Town tiles by Surt, modified by Blarumyrran.  Snow town tiles are a modification of the town tileset by Sharm.  Forest tiles contained in the town set are by Sharm.  Some of these works were commissioned by OpenGameArt.org (http://opengameart.org).

File(s):
    * classical_temple_tiles.png
    * tilesetformattedupdate1.png
    * town_snow.png
    * desert.png
    * cave.png
    * sewer_1.png
    * town_forest_tiles.png

----------------------------------------

Title:
    16x16 Item-Icons

Author:
    MrBeast

URL:
    https://opengameart.org/content/16x16-item-icons

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

File(s):
    * items.png

----------------------------------------

Title:
    16x16 Forest Tiles

Author:
    Sharm

Collaborators:
    surt, MrBeast

URL:
    https://opengameart.org/content/16x16-forest-tiles

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * CC-BY-SA 3.0 ( http://creativecommons.org/licenses/by-sa/3.0/legalcode )

File(s):
    * foresttiles.gif

----------------------------------------

Title:
    Castles

Author:
    Blarumyrran

URL:
    https://opengameart.org/content/castles

License(s):
    * CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )

File(s):
    * castles.png

----------------------------------------

Title:
    Ruining tower

Author:
    Blarumyrran

URL:
    https://opengameart.org/content/ruining-tower

License(s):
    * CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )

File(s):
    * towerruin_1x_transp.png
    * towerruin_1x.png

----------------------------------------

Title:
    Treasure chests, 32x32 and 16x16

Author:
    Blarumyrran

URL:
    https://opengameart.org/content/treasure-chests-32x32-and-16x16

License(s):
    * CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )

File(s):
    * chests_32x32.png
    * chests_16x16_0.png

----------------------------------------

Title:
    16x16 fantasy pixel art vehicles

Author:
    DualR

URL:
    https://opengameart.org/content/16x16-fantasy-pixel-art-vehicles

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * CC-BY-SA 3.0 ( http://creativecommons.org/licenses/by-sa/3.0/legalcode )
    * GPL 3.0 ( http://www.gnu.org/licenses/gpl-3.0.html )
    * GPL 2.0 ( http://www.gnu.org/licenses/old-licenses/gpl-2.0.html )

Copyright/Attribution Notice:
    Art by DualR. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * fullsheet.png
    * fullsheet.gif

----------------------------------------

Title:
    16x16 indoor rpg tileset: the baseline

Author:
    Stephen Challener (Redshrike)

Collaborators:
    bart

URL:
    https://opengameart.org/content/16x16-indoor-rpg-tileset-the-baseline

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * OGA-BY 3.0 ( http://static.opengameart.org/OGA-BY-3.0.txt )

Copyright/Attribution Notice:
    Art by Stephen Challener (Redshrike). Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * tilesetformattedupdate1.png
    * 16x16extratiles.PNG

----------------------------------------

Title:
    Worldmap/Overworld tileset

Author:
    MrBeast

URL:
    https://opengameart.org/content/worldmapoverworld-tileset

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Art by MrBeast. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * tileset.png
    * mountains.png
    * water.png

----------------------------------------

Title:
    Sewer tileset

Author:
    MrBeast

URL:
    https://opengameart.org/content/sewer-tileset

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * GPL 2.0 ( http://www.gnu.org/licenses/old-licenses/gpl-2.0.html )

Copyright/Attribution Notice:
    Art by MrBeast. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * sewer_1.png

----------------------------------------

Title:
    Desert Tileset

Author:
    MrBeast

URL:
    https://opengameart.org/content/desert-tileset-0

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Art by MrBeast. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * desert.png

----------------------------------------

Title:
    Cave tileset

Author:
    MrBeast

URL:
    https://opengameart.org/content/cave-tileset-0

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * GPL 2.0 ( http://www.gnu.org/licenses/old-licenses/gpl-2.0.html )

Copyright/Attribution Notice:
    Art by MrBeast. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * cave.png

----------------------------------------

Title:
    16x16 pixel art dungeon wall and cobblestone floor tiles

Author:
    Daniel Siegmund

URL:
    https://opengameart.org/content/16x16-pixel-art-dungeon-wall-and-cobblestone-floor-tiles

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * CC-BY-SA 3.0 ( http://creativecommons.org/licenses/by-sa/3.0/legalcode )
    * GPL 3.0 ( http://www.gnu.org/licenses/gpl-3.0.html )
    * GPL 2.0 ( http://www.gnu.org/licenses/old-licenses/gpl-2.0.html )

Copyright/Attribution Notice:
    Art by Daniel Siegmund and commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * wall2.png
    * cobbles2.png

----------------------------------------

Title:
    16x16 Castle Tiles

Author:
    bart

URL:
    https://opengameart.org/content/16x16-castle-tiles

License(s):
    * GPL 2.0 ( http://www.gnu.org/licenses/old-licenses/gpl-2.0.html )
    * GPL 3.0 ( http://www.gnu.org/licenses/gpl-3.0.html )
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * CC-BY-SA 3.0 ( http://creativecommons.org/licenses/by-sa/3.0/legalcode )

File(s):
    * 16x16 castle_0.png

----------------------------------------

Title:
    Town Tiles

Author:
    surt

URL:
    https://opengameart.org/content/town-tiles

License(s):
    * CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )

File(s):
    * town_tiles.png

----------------------------------------

Title:
    16x16 Snowy Town Tiles

Author:
    Sharm

Collaborators:
    surt

URL:
    https://opengameart.org/content/16x16-snowy-town-tiles

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * CC-BY-SA 3.0 ( http://creativecommons.org/licenses/by-sa/3.0/legalcode )
    * GPL 3.0 ( http://www.gnu.org/licenses/gpl-3.0.html )
    * GPL 2.0 ( http://www.gnu.org/licenses/old-licenses/gpl-2.0.html )

File(s):
    * snowtiles.gif

----------------------------------------

Title:
    Arctic Tileset

Author:
    Scribe

URL:
    https://opengameart.org/content/arctic-tileset

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

File(s):
    * ice tileset.png
    * inuit idle.gif
    * inuit walk.gif

----------------------------------------

Title:
    16x16, 16x24, 32x32 rpg enemies--updated

Author:
    Redshrike

URL:
    https://opengameart.org/content/16x16-16x24-32x32-rpg-enemies-updated

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * OGA-BY 3.0 ( http://static.opengameart.org/OGA-BY-3.0.txt )

Copyright/Attribution Notice:
    Stephen Challener (Redshrike), hosted by OpenGameArt.org

File(s):
    * rpgcritter update formatted transparent.png

----------------------------------------

Title:
    16x18 Attack stances

Author:
    CharlesGabriel

URL:
    https://opengameart.org/content/16x18-attack-stances

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Art by Charles Gabriel. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * OGA_16x18_charas_poses_Attack sheet.png
    * OGA_16x18_charas_poses_Attack sheet_AC.png
    * OGA_16x18_charas_poses_Attack sheet -Clothing.png
    * OGA_16x18_charas_poses_Attack sheet -Clothing_AC.png
    * OGA_16x18_charas_poses_template_Attack_A&C_AC.png

----------------------------------------

Title:
    16x18 Magic Cast Animations Sheet 2

Author:
    CharlesGabriel

URL:
    https://opengameart.org/content/16x18-magic-cast-animations-sheet-2

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Art by Charles Gabriel. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * OGA_16x18_charas_poses_template_Magic_cast_A&C.png
    * OGA_16x18_charas_poses_template_Magic_cast_A&C_AC.png
    * OGA_16x18_charas_poses_Magic_cast_sheet2_CP - Clothing.png
    * OGA_16x18_charas_poses_Magic_cast_sheet2_CP - Clothing_AC.png
    * OGA_16x18_charas_poses_Magic_cast_sheet2_CP.png
    * OGA_16x18_charas_poses_Magic_cast_sheet2_CP_AC.png

----------------------------------------

Title:
    16x18 Magic Cast Animations Sheet 1 + Template

Author:
    CharlesGabriel

URL:
    https://opengameart.org/content/16x18-magic-cast-animations-sheet-1-template

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Art by Charles Gabriel. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * OGA_16x18_charas_poses_Magic_cast_sheet1_CP.png
    * OGA_16x18_charas_poses_Magic_cast_sheet1_CP - Clothing-CP.png
    * OGA_16x18_charas_poses_Magic_cast_sheet1_CP_AC.png
    * OGA_16x18_charas_poses_Magic_cast_sheet1_CP-Clothing_AC.png
    * OGA_16x18_charas_poses_template_Magic_cast_A&C.png
    * OGA_16x18_charas_poses_template_Magic_cast_A&C_AC.png

----------------------------------------

Title:
    16x18 Zombie Characters + Templates + Extra Template

Author:
    CharlesGabriel

URL:
    https://opengameart.org/content/16x18-zombie-characters-templates-extra-template

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Art by Charles Gabriel. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * OGA_16x18 zombie charas complete.png

----------------------------------------

Title:
    Objects for 16x16 tilesets

Author:
    MrBeast

URL:
    https://opengameart.org/content/objects-for-16x16-tilesets

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )
    * GPL 2.0 ( http://www.gnu.org/licenses/old-licenses/gpl-2.0.html )

Copyright/Attribution Notice:
    Art by MrBeast. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * objects.png

----------------------------------------

Title:
    16x16 tileset: water, grass and sand

Author:
    Demetrius

URL:
    https://opengameart.org/content/16x16-tileset-water-grass-and-sand

License(s):
    * CC0 ( http://creativecommons.org/publicdomain/zero/1.0/legalcode )

File(s):
    * voda_pesok_trava_revision_2.png
    * voda_pesok_trava.png

----------------------------------------

Title:
    Twelve more 16x18 RPG character sprites

Author:
    Antifarea

URL:
    https://opengameart.org/content/twelve-more-16x18-rpg-character-sprites

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Art by Charles Gabriel. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * 16x18-2_0.png

----------------------------------------

Title:
    Twelve 16x18 RPG sprites, plus base

Author:
    Antifarea

URL:
    https://opengameart.org/content/twelve-16x18-rpg-sprites-plus-base

License(s):
    * CC-BY 3.0 ( http://creativecommons.org/licenses/by/3.0/legalcode )

Copyright/Attribution Notice:
    Art by Charles Gabriel. Commissioned by OpenGameArt.org (http://opengameart.org)

File(s):
    * charsets_12_m-f_complete_by_antifarea.png

----------------------------------------


